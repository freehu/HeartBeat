<%--
 * 
 * @author Shengzhao Li
--%>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE HTML>
<html>
<head>
    <title>用户注册被禁止</title>
</head>
<body>

<div class="row">
    <div class="col-md-12">
        <div class="alert alert-danger">
            <div>
                <h4><em class="fui-info-circle"></em> OOps... </h4>
                当前系统设置为禁止用户注册

                <br/>
                <br/>

                <a href="javascript:history.back();">返回</a>
            </div>
        </div>
    </div>
</div>


</body>
</html>